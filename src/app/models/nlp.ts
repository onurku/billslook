

export enum NLPDocumentType {
  TYPE_UNSPECIFIED,
  PLAIN_TEXT,
  HTML
}

export enum NLPEncodingType {
  NONE,
  UTF8,
  UTF16,
  UTF32
}

export class NLPDocument {
  type: NLPDocumentType;
  language: string;
  content: string;
}

export class NLPFeatures {
  extractSyntax: boolean;
  extractEntities: boolean;
  extractDocumentSentiment: boolean;
}

export class NLPSpeechAnalysis {
  json: string;
  tags: NLPSpeechAnalysisTag[];
  sentiment: NLPSpeechAnalysisSentiment;

  constructor(data: any) {
    this.json = JSON.stringify(data, null, 2);
    this.sentiment = {
      score: data.documentSentiment.score,
      magnitude: data.documentSentiment.magnitude
    }
    this.tags = [];

    for (let entity of data.entities) {
      if (entity.name !== 'name') {
        this.tags.push(<NLPSpeechAnalysisTag>{
          name: entity.name,
          type: entity.type,
          wikipedia_url: entity.metadata.wikipedia_url
        });
      }
    }
  }
}

export class NLPSpeechAnalysisTag {
  name: string;
  type: string;
  wikipedia_url: string;
}

export class NLPSpeechAnalysisSentiment {
  score: number;
  magnitude: number;
}
